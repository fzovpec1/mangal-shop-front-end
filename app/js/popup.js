function show_advert(){
    $('.daily_product').toggle();
    $('.background').toggle();
}
function show_advert_2(){
    $('.promotion').toggle();
    $('.background').toggle();
}
function show_advert_3(){
    $('.form_something').toggle();
    $('.background').toggle();
}
function show_advert_4(){
    $('.fight').toggle();
    $('.background-shop').toggle();
}
$(document).ready(function() {
    $('.daily_product__cross img').click(function() {
        $('.daily_product').toggle();
        $('.background').toggle();
        setTimeout(show_advert_2, 300000);
    })
    $('.hide__cross img').click(function() {
        $('.promotion').toggle();
        $('.background').toggle();
        setTimeout(show_advert_3, 660000);
    })
    $('.form_something__cross img').click(function() {
        $('.form_something').toggle();
        $('.background').toggle();
    })
    $('.header__menu').click(function() {
        $('.header__nav').toggle();
    })
    $('.shop-nav').click(function() {
        $('.sub__nav').toggle();
    })
    $('.contacts-nav').click(function() {
        $('.sub__nav__cont').toggle();
    })
    $('.srch').click(function() {
        $('.srch').toggle();
        $('.hide-search').toggle();
        $('.labelx').toggle();
    })
    $('.fight .cross').click(function() {
        $('.fight').toggle();
        $('.background-shop').toggle();
    })
    $('#hide_first').click(function() {
        $('.first_cart').toggle();
        $('.second_cart').toggle();
    })
    $('#hide_second').click(function() {
        $('.second_cart').toggle();
        $('.third_cart').toggle();
    })
    $('.header__cart').click(function() {
        $('.cart_set').toggle();
    })


});


setTimeout(show_advert, 180000);
setTimeout(show_advert_4, 180000);
